##############################################################################
#
#  KURS: 1DT038 2018.  Computer Architecture
#	
# DATUM:
#
#  NAMN: Viktor Lönnborg			
#
#  NAMN: Erik Karlsson
#
##############################################################################

	.data
	
ARRAY_SIZE:
	.word	10	# Change here to try other values (less than 10)
FIBONACCI_ARRAY:
	.word	1, 1, 2, 3, 5, 8, 13, 21, 34, 55
STR_str:
	.asciiz "Hunden, Katten, Glassen"

	.globl DBG
	.text

##############################################################################
#
# DESCRIPTION:  For an array of integers, returns the total sum of all
#		elements in the array.
#
# INPUT:        $a0 - address to first integer in array.
#		$a1 - size of array, i.e., numbers of integers in the array.
#
# OUTPUT:       $v0 - the total sum of all integers in the array.
#
##############################################################################
integer_array_sum:  

#DBG:	##### DEBUGG BREAKPOINT ######

        addi    $v0, $zero, 0           # Initialize Sum to zero.
	add	$t0, $zero, $zero	# Initialize array index i to zero.
        addi    $t1, $zero, -4
for_all_in_array:

	#### Append a MIPS-instruktion before each of these comments
	

       beq 	$t0, $a1, end_for_all	# Done if i == N
       addi	 $t1, $t1, 4  		# 4*i
       add 	$t2, $a0, $t1 		# address = ARRAY + 4*i
       lw 	$t3, 0($t2) 		# n = A[i]
       add 	$v0, $v0, $t3 		# Sum = Sum + n
       addi 	$t0, $t0, 1   		# i++
       j 	for_all_in_array        # next element
	
end_for_all:
	
	jr	$ra			# Return to caller.
	
##############################################################################
#
# DESCRIPTION: Gives the length of a string.
#
#       INPUT: $a0 - address to a NUL terminated string.
#
#      OUTPUT: $v0 - length of the string (NUL excluded).
#
#    EXAMPLE:  string_length("abcdef") == 6.
#
##############################################################################	


string_length: #arg1 : Null term string

        addiu 	$v0, $0, 0x00  # index
	li 	$t3, 0x00      # load a temporary register with value 0
        move 	$t4, $a0       # stores the address of the given string in a temporary register
        li      $v0, 0         # load a temporary register with value 0

str_len_loop:
	
	lb 	$t1, 0($t4)     	     # Load Byte at adress1
	
	beq 	$t1, $zero, str_len_loop_end # Byte is 0
        la 	$t4, 0x1($t4)                # loads the address with an offset 1
	addi 	$v0, $v0, 1                  # Length + 1
	j 	str_len_loop                 # jump to the loop label
str_len_loop_end:	
	jr	$ra
	


	
##############################################################################
#
#  DESCRIPTION: For each of the characters in a string (from left to right),
#		call a callback subroutine.
#
#		The callback suboutine will be called with the address of
#	        the character as the input parameter ($a0).
#	
#        INPUT: $a0 - address to a NUL terminated string.
#
#		$a1 - address to a callback subroutine.
#
##############################################################################	
string_for_each:

	addi	$sp, $sp, -4	# PUSH return address to caller
	sw	$ra, 0($sp)
	sw	$s0, -4($sp)

	move 	$s0, $a0	# stores the address in a saved register
loop_str_for_each:
        lb 	$t1, 0($s0)
        beq 	$t1, $zero, loop_str_for_each_end   #skip if the character is the end of the string 

	move 	$a0, $s0                           
	jal 	$a1		# jumps and links a callback subroutine                      

        la	$s0, 0x1($s0)
        j	loop_str_for_each

loop_str_for_each_end:
        li	$s0, 0		# restores the saved register into it's original value
	lw	$s0, -4($sp)
        lw	$ra, 0($sp)	# Pop return address to caller
	addi	$sp, $sp, 4
	jr	$ra


###############################################################################
#
#  DESCRIPTION: Reverses the order of a given NUL terminated string.
#
#        INPUT: $a0 - address to a NUL terminated string.
#
##############################################################################

string_reverse:
    addi    	$sp, $sp, -4	# PUSH return address to caller
    sw	    	$ra, 0($sp)
    jal     	string_length	# Get length of string

    
    li      	$t8, 1
    move    	$t1, $v0	# Length of string
    move    	$v0, $zero	# Reset return value to zero
    move    	$t0, $zero	# Index 0 of string

    srl 	$t6, $t1, 1	# divide the length 2
    and 	$t7, $t1, $t8
    add 	$t6, $t6, $t7	# + 1 if the length f the string is even

string_reverse_loop:

    ble 	$t6, $t0, string_reverse_end # if the index is at half the length of the string then end the loop
    add     	$t2, $a0, $t0	# adress of char with offset n
    lb     	$t3, 0($t2)	# char with offset n

    sub     	$t4, $t1, $t0	# string length - n
    addi    	$t4, $t4, -1
    add     	$t4, $a0, $t4	# adress of (string length - n)
    lb     	$t5, 0($t4)	# char with offset (string length - n)

    sb 		$t5, 0($t2)	# store char with offset (string length - n) at n
    sb 		$t3, 0($t4)	# store char with offset n at (string len - n)

    addi     	$t0, $t0, 1	# n += 1 
    j     	string_reverse_loop     # loop


string_reverse_end:
    lw		$ra, 0($sp)	# Pop return address to caller
    addi	$sp, $sp, 4
    jr 		$ra

##############################################################################
#
#  DESCRIPTION: Transforms a lower case character [a-z] to upper case [A-Z].
#	
#        INPUT: $a0 - address of a character 
#
##############################################################################			# dec 97-122 are lower chars
to_upper:

	lbu 	$t0, ($a0)            # loads the first character in a given string in a temporary register
	addi 	$t1, $t0, -96         # n - 96
	blez 	$t1, else             # if n is less than zero then else
	addi 	$t1, $t1, 25          # n + 25
	blez 	$t1, else             # if n is less than zero then else
	

	addi 	$t0, $t0, -0x20	      # n - 20
	sb 	$t0, ($a0)            # stores the new ASCII value for the given lower case character

else:		
	jr	$ra


##############################################################################
#
# Strings used by main:
#
##############################################################################

	.data

NLNL:	.asciiz "\n\n"


STR_rev_show:
	.asciiz "\n\nreversed('hello')='"
STR_rev_show_end:
	.asciiz "'\n"
STR_rev_str:
	.asciiz "hello"
STR_sum_of_fibonacci_a:	
	.asciiz "The sum of the " 
STR_sum_of_fibonacci_b:
	.asciiz " first Fibonacci numbers is " 
STR_string_length:
	.asciiz	"\n\nstring_length(str) = "

STR_for_each_ascii:	
	.asciiz "\n\nstring_for_each(str, ascii)\n"

STR_for_each_to_upper:
	.asciiz "\n\nstring_for_each(str, to_upper)\n\n"	

	.text
	.globl main

##############################################################################
#
# MAIN: Main calls various subroutines and print out results.
#
##############################################################################	
main:
	addi	$sp, $sp, -4	# PUSH return address
	sw	$ra, 0($sp)

	##
	### integer_array_sum
	##
	
	li	$v0, 4
	la	$a0, STR_sum_of_fibonacci_a
	syscall

	lw 	$a0, ARRAY_SIZE
	li	$v0, 1
	syscall

	li	$v0, 4
	la	$a0, STR_sum_of_fibonacci_b
	syscall
	
	la	$a0, FIBONACCI_ARRAY
	lw	$a1, ARRAY_SIZE
	jal 	integer_array_sum

	# Print sum
	add	$a0, $v0, $zero
	li	$v0, 1
	syscall

	li	$v0, 4
	la	$a0, NLNL
	syscall
	
	la	$a0, STR_str
	jal	print_test_string

	##
	### string_length 
	##
	
	li	$v0, 4
	la	$a0, STR_string_length
	syscall

	la	$a0, STR_str
	jal 	string_length

	add	$a0, $v0, $zero
	li	$v0, 1
	syscall

	##
	### string_for_each(string, ascii)
	##
	
	li	$v0, 4
	la	$a0, STR_for_each_ascii
	syscall
	
	la	$a0, STR_str
	la	$a1, ascii
	jal	string_for_each

	##
	### string_for_each(string, to_upper)
	##
	
	li	$v0, 4
	la	$a0, STR_for_each_to_upper
	syscall

	la	$a0, STR_str
	la	$a1, to_upper
	jal	string_for_each
	
	la	$a0, STR_str
	jal	print_test_string

	##
	### string_reversed
	##
	
	li	$v0, 4
	la	$a0, STR_rev_show
	syscall

	la	$a0, STR_rev_str

	jal 	string_reverse	# Call reverse_string


	li	$v0, 4
	la	$a0, STR_rev_str
	syscall

	li	$v0, 4
	la	$a0, STR_rev_show_end
	syscall
	
	lw	$ra, 0($sp)	# POP return address
	addi	$sp, $sp, 4	
	
	jr	$ra

##############################################################################
#
#  DESCRIPTION : Prints out 'str = ' followed by the input string surronded
#		 by double quotes to the console. 
#
#        INPUT: $a0 - address to a NUL terminated string.
#
##############################################################################
print_test_string:	

	.data
STR_str_is:
	.asciiz "str = \""
STR_quote:
	.asciiz "\""	

	.text

	add	$t0, $a0, $zero
	
	li	$v0, 4
	la	$a0, STR_str_is
	syscall

	add	$a0, $t0, $zero
	syscall

	li	$v0, 4	
	la	$a0, STR_quote
	syscall
	
	jr	$ra
	

##############################################################################
#
#  DESCRIPTION: Prints out the Ascii value of a character.
#	
#        INPUT: $a0 - address of a character 
#
##############################################################################
ascii:	
	.data
STR_the_ascii_value_is:
	.asciiz "\nAscii('X') = "

	.text

	la	$t0, STR_the_ascii_value_is

	# Replace X with the input character
	
	add	$t1, $t0, 8	# Position of X
	lb	$t2, 0($a0)	# Get the Ascii value
	sb	$t2, 0($t1)

	# Print "The Ascii value of..."
	
	add	$a0, $t0, $zero 
	li	$v0, 4
	syscall

	# Append the Ascii value
	
	add	$a0, $t2, $zero
	li	$v0, 1
	syscall


	jr	$ra
	
