#= Sum of array with size =#
# Written by Erik Karlsson #
# Under WTFPL              #
# ->(www.wtfpl.com)        #
#===# Happy Hacking! #=====#


#LANG: MIPS Assembly	         
#      with pseudo instructions
				    
#TESTED: In spim from (AUR) with default settings allowing
#        pseudo-ops, no delayed branches and loads          

	
	
##################### Storing of Values and Debug ############################

        .data
array:	.word 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 # words of amount size 
size:	.word 10 # Size of array

PREFIX:	.asciiz "sum(array, size) = " # Print before sum
SUBFIX:	.asciiz "\n"                  #

	.globl init        # Provide init for main  
	.globl adress      # Debug for breakpoints
	.globl value       # Debug for breakpoints
	.globl end_for_all # Debug for breakpoints
	
##############################################################################




##################### Routine  Declaration ###################################

	.text # End storing values
j main # Skip  pre-evaluation of routines
	
init: # Initialize values
	add $v0, $0, $0  # Init value
        addi $t0, $0, -1 # Init value

	# -1 because (1) is calculated at the end of
	# for_all_in_array
	
for_all_in_array: # for i=0 until i=ARG1 do Retval += ARG2[i] 	
	beq $t0, $a1, end_for_all #  if (index == array_size) return sum[array] 
	 mul $t1, $t0, 4 # offset = offset * index 
         add $t2, $t1, $a0 # next_adress = adress[0] + offset 
	lw $t3, 0($t2) # adress_value = adress[next_adress]
adress: # Point after adress load
	
	add $v0, $v0, $t3 # Sum = Sum + adress_value
value: # Point after Sum update
	
	addu $t0, $t0, 1 #(1) index = index + 1
	j for_all_in_array # Repeat

end_for_all: # Return and reset temps 
        addi $t0, $0, 0
        addi $t1, $0, 0
	addi $t2, $0, 0
	addi $t3, $0, 0
	jr $ra # Return to caller.
##############################################################################



	
######################## Main routine, Prints sum(array, size) ###############

#void main(){
main:	# I do not need to load $sp to stack
	# due to only jumping and linking once from main
	# which also is where is sigterm exit

# word temp = sum(array, size)	
	la $a0, array
        lw $a1, size
	lw $ra, main
        jal init # Call method init

# word num = temp 
        add $s1, $v0, $0 # Save return value

# printf("%s", PREFIX)
	la $a0, PREFIX
	li $v0, 4
	syscall # Print description of init

# printf("%n", *num)
	add $a0, $s1, $0
	li $v0, 1
	syscall # Print return number from init

# printf("%s", *SUBFIX)
        la $a0, SUBFIX
	li $v0, 4
	syscall # Print newline
# return void}
	li $v0, 10
	syscall # Exit
##############################################################################
